import React from 'react'
import { StyleSheet, Text, View , FlatList, TouchableOpacity } from 'react-native'
import {useNavigation} from '@react-navigation/native'
import obtenerEstablecimiento from '../screens/DetallesEstablecimientos'

const FlatListNegocios = (props) => {
  const navigation = useNavigation();
  const {negocioh} = props
  
  return (
    <View>
      <FlatList
        data = {negocioh}
        renderItem = { (negocio) => <Item negocio = {negocio} navigation = {navigation}/> }
        keyExtractor = {(index) => index.toString() }
        //ItemSeparatorComponent={this.renderSeparator}
      />
    </View>
  )
}
renderSeparator = () => {
  return(
    <View
    style={{height:1, width:'100%',backgroundColor:'black'}}>

    </View>
  )
}
const Item = (props) => {
  const {negocio,navigation} = props;
  const {Nombre,idEstablecimiento} = negocio.item;
  
  return (
    <TouchableOpacity style={{flex:1, flexDirection:'row', marginBottom:3}}

    onPress={() => navigation.navigate("DetallesNegocios",{idEstablecimiento:idEstablecimiento}) }>
    <View style={{flex:1, justifyContent:'center', margin:5}}>
      <Text style={{ fontSize: 18, color: 'black', fontWeight: "bold",}}>{Nombre}</Text>

      </View>
    </TouchableOpacity>
  )
}

export default FlatlistEstablecimiento
