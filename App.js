
import React from "react";
import { StyleSheet } from "react-native";


import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack';

import ListarNegocios from "./app/screens/Negocios/ListarNegocios";
import DetallesNegocios from "./app/screens/Negocios/DetallesNegocios";

const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: "#621FF7",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontWeight: "bold",
        },
      }}
    >
      <Stack.Screen
        name="listar-negocios"
        component={ListarNegocios}
        options={{ title: "Listado de negocios asociados" }}
      />
      <Stack.Screen
        name="detalles-negocios"
        component={DetallesNegocios}
        options={{ title: "Detalles de negocio" }}
      />
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
